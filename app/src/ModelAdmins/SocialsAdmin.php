<?php

namespace {

    use SilverStripe\Admin\ModelAdmin;

    class SocialsAdmin extends ModelAdmin
    {
        private static $managed_models = [
            Socials::class
        ];

        private static $url_segment = 'socials';
        private static $menu_title = 'Socials';
        private static $menu_icon_class = 'font-icon-chat';
    }
}
