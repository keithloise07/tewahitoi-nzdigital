<?php

namespace {

    use SilverStripe\Admin\ModelAdmin;

    class ContentBuilderAdmin extends ModelAdmin
    {
        private static $managed_models = [
            ContentBuilder::class
        ];

        private static $url_segment = 'contentbuilder';
        private static $menu_title = 'Content Builder';

        private static $menu_icon_class = 'font-icon-block-layout-4';
    }
}
