<?php

namespace {

    use SilverStripe\Admin\ModelAdmin;

    class ListingsAdmin extends ModelAdmin
    {
        private static $managed_models = [
            Listings::class
        ];

        private static $url_segment = 'listings';
        private static $menu_title = 'Listings';
        private static $menu_icon_class = 'font-icon-block-content';
    }
}
