<?php

namespace {

    use SilverStripe\Admin\ModelAdmin;

    class SearchTabAdmin extends ModelAdmin
    {
        private static $managed_models = [
            SearchTabs::class
        ];

        private static $url_segment = 'searchtab';
        private static $menu_title = 'Search Tab';
        private static $menu_icon_class = 'font-icon-block-search';
    }
}
