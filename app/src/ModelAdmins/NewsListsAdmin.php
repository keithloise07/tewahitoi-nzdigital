<?php

namespace {

    use SilverStripe\Admin\ModelAdmin;

    class NewsListsAdmin extends ModelAdmin
    {
        private static $managed_models = [
            NewsList::class
        ];

        private static $url_segment = 'newslists';
        private static $menu_title = 'News Lists';
        private static $menu_icon_class = 'font-icon-news';
    }
}
