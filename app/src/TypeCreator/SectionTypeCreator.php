<?php

namespace Tewahitoi\GraphQL;

use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\TypeCreator;

class SectionTypeCreator extends TypeCreator
{
    public function attributes()
    {
        return [
            'name' => 'Section',
            'content' => 'Add a content here'
        ];
    }

    public function fields()
    {
        return [
            'Name' => ['type' => Type::string()],
            'Content' => ['type' => Type::string()],
            'SectionType' => ['type' => Type::string()],
        ];
    }
}
