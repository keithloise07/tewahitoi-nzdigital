<?php

namespace {

    class ListingHolderPage extends Page
    {
        private static $icon_class = 'font-icon-menu-campaigns';

        private static $allowed_children = [
            ListingPage::class
        ];

        private static $can_be_root = false;
    }
}
