<?php

namespace {

    use SilverStripe\Control\HTTPRequest;

    class AjaxController extends AbstractApiController
    {
        private static $allowed_actions = [
            'myfirstajax',
        ];

        public function myfirstajax(HTTPRequest $request)
        {

            return $this->jsonOutput();
        }

    }
}
