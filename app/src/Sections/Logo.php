<?php

namespace {

    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use Symbiote\GridFieldExtensions\GridFieldEditableColumns;
    use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

    class Logo extends Section
    {
        private static $singular_name = 'Logo';

        private static $has_many = [
            'LogoItems' => LogoItem::class
        ];

        public function getSectionCMSFields(FieldList $fields)
        {
            $gridConfig = GridFieldConfig_RecordEditor::create(999);
            if($this->LogoItems()->Count())
            {
                $gridConfig->addComponent(new GridFieldOrderableRows());
            }
            $gridConfig->addComponent(new GridFieldEditableColumns());
            $gridColumns = $gridConfig->getComponentByType(GridFieldEditableColumns::class);
            $gridColumns->setDisplayFields([
                'Archived' => [
                    'title' => 'Archive',
                    'callback' => function($record, $column, $grid) {
                        return CheckboxField::create($column);
                    }]
            ]);

            $gridField = GridField::create(
                'LogoItems',
                'Logo Items',
                $this->LogoItems(),
                $gridConfig
            );

            $fields->removeByName("LogoItems");
            $fields->addFieldToTab('Root.Main', $gridField);
        }

        public function getVisibleLogoItems()
        {
            return $this->LogoItems()->filter('Archived', false)->sort('Sort');
        }
    }
}
