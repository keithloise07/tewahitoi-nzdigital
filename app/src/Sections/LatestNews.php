<?php

namespace {

    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\ListboxField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\TreeDropdownField;

    class LatestNews extends Section
    {
        private static $singular_name = 'Latest News';

        private static $db = [
            'Content'       => 'HTMLText',
            'ButtonText'    => 'Text',
            'ButtonDisable' => 'Boolean',
        ];

        private static $has_one = [
            'Image'    => Image::class,
            'PageLink' => SiteTree::class
        ];

        private static $owns = [
           'Image',
        ];

        private static $many_many = [
            'NewsLists' => NewsList::class
        ];

        public function getSectionCMSFields(FieldList $fields)
        {
            $fields->addFieldToTab('Root.Main', HTMLEditorField::create('Content'));
            $fields->addFieldToTab('Root.Main', $image = UploadField::create('Image'));
            $image->setFolderName('Sections/LatestNews/Images');
            $image->getValidator()->setAllowedExtensions(['png','gif','jpeg','jpg']);
            $fields->addFieldToTab('Root.Main', ListboxField::create('NewsLists', 'Select a news', NewsList::get()->filter('Archived', false)->map('ID', 'Name')));
            $fields->addFieldToTab('Root.Main', TreeDropdownField::create('PageLinkID', 'Page link', SiteTree::class)
                ->setDescription('Selecting a Page will add link to the button'));
            $fields->addFieldToTab('Root.Main', TextField::create('ButtonText', 'Button text'));
            $fields->addFieldToTab('Root.Main', CheckboxField::create('ButtonDisable', 'Disable button'));
        }
    }
}
