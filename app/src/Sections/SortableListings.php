<?php

namespace {

    use SilverStripe\Forms\DropdownField;
    use SilverStripe\Forms\FieldList;

    class SortableListings extends Section
    {
        private static $singular_name = 'Sortable Listing';

        private static $has_one = [
            'SearchTabItem' => SearchTabItem::class,
        ];

        public function getSectionCMSFields(FieldList $fields)
        {
            $fields->addFieldToTab('Root.Main', DropdownField::create('SearchTabItemID', 'Type',
                SearchTabItem::get()->filter('Archived', false)->map('ID', 'Name')));
        }
    }
}
