<?php

namespace {

    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    use SilverStripe\Forms\DropdownField;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

    class ImageBanner extends Section
    {
        private static $singular_name = 'Image Banner';

        private static $db = [
            'Content'         => 'HTMLText',
            'ContentPosition' => 'Varchar',
            'ImageHeight'     => 'Varchar',
        ];

        private static $has_one = [
            'Image' => Image::class
        ];

        private static $owns = [
            'Image'
        ];

        private static $defaults = [
            'ImageHeight' => 'bh-large'
        ];

        public function getSectionCMSFields(FieldList $fields)
        {
            $fields->addFieldToTab('Root.Main', $image = UploadField::create('Image', 'Banner image'));
            $image->setFolderName('Sections/Section_Banner/Images');
            $image->setAllowedExtensions(['png','gif','jpeg','jpg']);
            $fields->addFieldToTab('Root.Main', DropdownField::create('ImageHeight', 'Image height',
                array(
                    'bh-small' => 'Small',
                    'bh-medium'=> 'Medium',
                    'bh-large' => 'Large',
                    'bh-xlarge'=> 'XLarge'
                )
            ));
            $fields->addFieldToTab('Root.Main', HTMLEditorField::create('Content'));
            $fields->addFieldToTab('Root.Main', DropdownField::create('ContentPosition', 'Content position',
                array(
                    'cp-left'   => 'Left',
                    'cp-center' => 'Center',
                    'cp-right'  => 'Right'
                )
            ));
        }
    }
}
