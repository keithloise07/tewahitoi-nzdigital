<?php

namespace {

    use SilverStripe\Forms\DropdownField;
    use SilverStripe\Forms\FieldList;

    class SearchTabElement extends Section
    {
        private static $singular_name = 'Search Tab Element';

        private static $has_one = [
            'SearchTab' => SearchTabs::class
        ];

        public function getSectionCMSFields(FieldList $fields)
        {
            $fields->addFieldToTab('Root.Main', DropdownField::create('SearchTabID', 'Search tab',
                SearchTabs::get()->filter('Archived', false)->map('ID', 'Name')));
        }
    }
}
