<?php

namespace {

    use Sheadawson\DependentDropdown\Forms\DependentListboxField;
    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\DropdownField;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\TreeDropdownField;

    class ListingGrid extends Section
    {
        private static $singular_name = 'Listing Grid';

        private static $db = [
            'Content'      => 'HTMLText',
            'ButtonText'   => 'Text'
        ];

        private static $has_one = [
            'SearchTabItem' => SearchTabItem::class,
            'PageLink'      => SiteTree::class
        ];

        private static $many_many = [
            'Listings' => Listings::class
        ];

        public function getSectionCMSFields(FieldList $fields)
        {
            $fields->addFieldToTab('Root.Main', HTMLEditorField::create('Content'));
            $fields->addFieldToTab('Root.Main', $type = DropdownField::create('SearchTabItemID', 'Type',
                SearchTabItem::get()->filter('Archived', false)->map('ID', 'Name')));

            $listingsSource = function ($listings)
            {
                return Listings::get()->filter(['Archived' => false, 'TypeID' => $listings])->map('ID', 'Name');
            };

            $fields->addFieldToTab('Root.Main', DependentListboxField::create('Listings', 'Select a listing', $listingsSource)->setDepends($type));
            $fields->addFieldToTab('Root.Main', TreeDropdownField::create('PageLinkID', 'Page link', SiteTree::class)
                ->setDescription('Selecting a Page will add link to the button'));
            $fields->addFieldToTab('Root.Main', TextField::create('ButtonText', 'Button text'));
        }
    }
}
