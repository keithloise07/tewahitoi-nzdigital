<?php

namespace {

    use Sheadawson\DependentDropdown\Forms\DependentListboxField;
    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\DropdownField;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\TreeDropdownField;
    use function foo\func;

    class UpcomingEvents extends Section
    {
        private static $singular_name = 'Upcoming Events';

        private static $db = [
            'ButtonText' => 'Text'
        ];

        private static $has_one = [
            'SearchTabItem' => SearchTabItem::class,
            'PageLink'      => SiteTree::class
        ];

        private static $many_many = [
            'UpcomingEventItems' => Listings::class
        ];

        public function getSectionCMSFields(FieldList $fields)
        {
            $fields->addFieldToTab('Root.Main', $type = DropdownField::create('SearchTabItemID', 'Type',
                SearchTabItem::get()->filter('Archived', false)->map('ID', 'Name')));

            $listingsSource = function ($listings)
            {
                return Listings::get()->filter(['Archived' => false, 'TypeID' => $listings])->map('ID', 'Name');
            };

            $testlistingsSource = function ($listings)
            {
                $listings = Listings::get()->filter(['Archived' => false, 'TypeID' => $listings]);
                $currentDate = new DateTime();

                $dateArray = [];
                foreach ($listings as $listing) {
                    $dates = $listing->ListingDateAndTime();
                    if (count($dates)) {
                        foreach ($dates as $date) {
                            $dateArray[] = $date->Date;
                            //$output[] = $date->Date . ' ' . $currentDate->format('Y-m-d');
                        }
                        $values = $this->findClosestDateFromCurrentDate($dateArray, $currentDate);
                    }
                }

                return $dateArray;
            };

            $fields->addFieldToTab('Root.Main', $upcomingEventItemSelector = DependentListboxField::create('UpcomingEventItems', 'Select upcoming events', $listingsSource)->setDepends($type));
            $upcomingEventItemSelector->displayIf('ShowAllItems')->isNotChecked()->end();

            $fields->addFieldToTab('Root.Main', TreeDropdownField::create('PageLinkID', 'Page link', SiteTree::class)
                ->setDescription('Selecting a Page will add link to the button'));
            $fields->addFieldToTab('Root.Main', TextField::create('ButtonText', 'Button text'));
        }

        public function findClosestDateFromCurrentDate($date_array, $currentDate)
        {
            foreach ($date_array as $date) {
                if ($date >= $currentDate) {
                    return $date;
                }
            }
            return end($date_array);
        }
    }
}
