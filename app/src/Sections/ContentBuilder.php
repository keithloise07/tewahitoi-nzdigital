<?php

namespace {

    use SilverStripe\ORM\DataObject;

    class ContentBuilder extends DataObject
    {
        private static $default_sort = 'Sort';

        private static $singular_name = 'Content Section';
    }
}
