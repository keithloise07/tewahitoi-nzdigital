<?php

namespace {

    use Sheadawson\DependentDropdown\Forms\DependentListboxField;
    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\Forms\DropdownField;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\TreeDropdownField;

    class FeaturedEvents extends Section
    {
        private static $singular_name = 'Featured Events';

        private static $db = [
            'ButtonText'    => 'Text',
            'ButtonDisable' => 'Boolean',
        ];

        private static $has_one = [
            'SearchTabItem' => SearchTabItem::class,
            'PageLink'      => SiteTree::class
        ];

        private static $many_many = [
            'FeaturedItems' => Listings::class
        ];

        public function getSectionCMSFields(FieldList $fields)
        {
            $fields->addFieldToTab('Root.Main', $type = DropdownField::create('SearchTabItemID', 'Type',
                SearchTabItem::get()->filter('Archived', false)->map('ID', 'Name')));

            $listingsSource = function ($listings)
            {
                return Listings::get()->filter(['Archived' => false, 'TypeID' => $listings, 'isFeatured' => true])->map('ID', 'Name');
            };

            $fields->addFieldToTab('Root.Main', DependentListboxField::create('FeaturedItems', 'Select featured events', $listingsSource)->setDepends($type));
            $fields->addFieldToTab('Root.Main', TreeDropdownField::create('PageLinkID', 'Page link', SiteTree::class)
                   ->setDescription('Selecting a Page will add link to the button'));
            $fields->addFieldToTab('Root.Main', TextField::create('ButtonText', 'Button text'));
            $fields->addFieldToTab('Root.Main', CheckboxField::create('ButtonDisable', 'Disable button'));
        }
    }
}
