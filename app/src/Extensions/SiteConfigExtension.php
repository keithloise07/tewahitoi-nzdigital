<?php

namespace {

    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\File;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\ORM\DataExtension;
    use TractorCow\Colorpicker\Color;
    use TractorCow\Colorpicker\Forms\ColorField;
    use TractorCow\SliderField\SliderField;

    class SiteConfigExtension extends DataExtension
    {
        private static $db = [
            'LogoWidth'  => 'Int',
            'LogoWidthIpad'  => 'Int',
            'LogoWidthMobile'  => 'Int',
            'PreHeader'  => 'HTMLText',
            'PreHeaderMobileText' => 'HTMLText',
            'PreHeaderBg'=> Color::class
        ];

        private static $has_one = [
            'Logo' => File::class,
            'LogoScrolled' => File::class
        ];

        private static $owns = [
            'Logo',
            'LogoScrolled'
        ];

        public function updateCMSFields(FieldList $fields)
        {
            $fields->addFieldToTab('Root.Header', UploadField::create('Logo')->setFolderName('Logo'));
            $fields->addFieldToTab('Root.Header', UploadField::create('LogoScrolled', 'Logo when page scrolled down')->setFolderName('Logo'));
            $fields->addFieldToTab('Root.Header', SliderField::create('LogoWidth', 'Logo width', '50', '350'));
            $fields->addFieldToTab('Root.Header', SliderField::create('LogoWidthIpad', 'Logo width for ipad', '50', '350'));
            $fields->addFieldToTab('Root.Header', SliderField::create('LogoWidthMobile', 'Logo width for mobile', '50', '350'));
            $fields->addFieldToTab('Root.Header', HTMLEditorField::create('PreHeader', 'Pre-header text desktop'));
            $fields->addFieldToTab('Root.Header', HTMLEditorField::create('PreHeaderMobileText', 'Pre-header text mobile'));
            $fields->addFieldToTab('Root.Header', ColorField::create('PreHeaderBg','PreHeader bgcolor'));

            /*
             *  Section Width
             */
            $configWidth = GridFieldConfig_RecordEditor::create('999');
            $editorWidth = GridField::create('SectionWidth', 'Width', SectionWidth::get(), $configWidth);
            $fields->addFieldToTab('Root.Sections', $editorWidth);

            /*
             *  Section Padding
             */
            $configPadding = GridFieldConfig_RecordEditor::create('999');
            $editorPadding = GridField::create('SectionPadding', 'Padding', Paddings::get(), $configPadding);
            $fields->addFieldToTab('Root.Sections', $editorPadding);

            /*
             *  Footer
             */
            $configFooter = GridFieldConfig_RecordEditor::create('999');
            $editorFooter = GridField::create('Footer', 'Footer', Footer::get(), $configFooter);
            $fields->addFieldToTab('Root.Footer', $editorFooter);
        }
    }
}
