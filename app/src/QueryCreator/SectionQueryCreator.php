<?php

namespace Tewahitoi\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use SilverStripe\GraphQL\OperationResolver;
use SilverStripe\GraphQL\QueryCreator;

class SectionQueryCreator extends QueryCreator implements OperationResolver
{
    public function attributes()
    {
        return [
            'name' => 'readSection'
        ];
    }

    public function type()
    {
        return Type::listOf($this->manager->getType('section'));
    }

    public function resolve($object, array $args, $context, ResolveInfo $info)
    {
        $list = \Section::get();

        // Optional filtering by properties
        if (isset($args['Name'])) {
            $list = $list->filter('Name', $args['Name']);
        }

        return $list;
    }
}
